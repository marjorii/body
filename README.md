# Body Recoding

## Préambule

Avec pour point de départ la scène de transformation d’Usagi Tsukino en Sailor Moon, héroïne de l’anime japonais du même nom créé par Naoko Takeuchi en 1992, ce texte souhaite déconstruire les sommations assenées aux corps sexisés pour en révéler ensuite la puissance. De même que les cadrages en plan rapproché isolent des membres du corps au gré de la métamorphose (main, yeux, buste, pieds, etc.), le texte principal se découpe en sous-textes indépendants désignant ces mêmes parties du corps. Mais là où leur exaltation est au service du fantasme cismasculin hétérosexuel dans la série animée (surtout du fait de sa production), la forme d’adoration du corps proposée dans cet essai a pour premier destinataire le sujet regardé : c’est un culte personnel, pour soi. Se vénérer pour ranimer ces corps ordonnés à la passivité, des exemples choisis illustrent ces reconquêtes des corps, notamment par le recours à la technologie (au numérique plus spécifiquement), à l’expression artistique, à la fiction, ou au jeu, qui sont autant de substituts aux pouvoirs magiques des sailors.

Pour introduire l’écrit, le travail d’Érica Scourti paraît plus qu’approprié puisqu’il « peut être interprété comme une autobiographie performative, avec laquelle elle explore les identités à l’intérieur des systèmes bio-socio-technologiques contemporains ». La pièce vidéo *Body Scan* (2014) fonctionne comme une œuvre-écho avec le texte, tant par le sujet que par la forme : 
> « l’artiste a capturé des images de son corps, avec son smartphone et les a soumises à différents moteurs de recherche et applications qui tentent de corréler ces images avec des informations sur le Web. En voix off, l’artiste commente les résultats de la recherche et réfléchit aux significations parfois amusantes et souvent sexistes qu’ils contiennent. Par exemple, les images de certaines parties du corps des personnes sexisées (en particulier les seins) s’accompagnent systématiquement de suggestions sur la manière de les améliorer. \[…] Scourti s’intéresse également aux aspects normatifs encodés dans la recherche d’images, comme dans de nombreux autres processus algorithmiques. Body Scan rend visible l’objectivation et la standardisation des corps sexisés. Le travail est intime et autobiographique, mais fait également référence à des forces et des évolutions sociales beaucoup plus vastes ».[^1]

Le texte-corps, qui comprend l’ensemble de l’essai, est actualisé sur un [répertoire Nextcloud](https://marjorieober.com/nextcloud/s/We35Ndi4JEHYXt6), de même que les sous-textes (ou textes-membres) y sont publiés progressivement dans leurs sous-répertoires dédiés. Le texte global a été versionné et importé sur Gitlab afin de permettre son amendement et l’accès à différents stade de l’écriture : [texte live sur Gitlab](https://gitlab.com/marjorii/body).

## Érica Scourti, *Body Scan* (2014)

![Photographie d’une bouche en très gros plan](iconography/01.jpeg)  
Figure 1 : Lèvres de l’artiste en cours d'identification par l'algorithme

![Bouches “glamours” affichées dans un navigateur web](iconography/02.jpeg)  
Figure 2 : Suggestions de correspondances d’images par un moteur de recherche après analyse

[Vidéo de l’œuvre *Body Scan* (5:03 min) sur Vimeo](https://vimeo.com/111503640)

___
[^1]: Les citations sont extraites du livret accompagnant l’exposition Computer Grrrls, tenue du 14 mars au 14 juillet 2019 à la Gaïté Lyrique (Paris). Il fût d’une grande aide pour documenter aussi bien le contenu textuel qu’iconographique.
